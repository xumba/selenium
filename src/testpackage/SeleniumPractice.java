package testpackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumPractice {
        public static void main(String[] args) {
            String baseUrl = "https://nl.edreams.com";
            WebDriver driver;
            System.setProperty("webdriver.gecko.driver", "/Users/oksanaruban/Documents/Java/chromedriver"); //selenium 3.0

            driver = new FirefoxDriver();
            driver.manage().window().maximize();
            driver.get(baseUrl);
            driver.findElement(By.id("//div[@id='flights-manager']//div[@class='od-flightsManager-header wl-bookingcom-form-header']/ul/li[2]/span[@class='od-flightsManager-services-text']")).click();
            //driver.findElement(By.xpath(""));
            driver.quit();
        }
}

