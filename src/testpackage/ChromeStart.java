package testpackage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ChromeStart {
    public static void main(String[] args) {
        String baseUrl = "https://nl.edreams.com";
        WebDriver driver;
        System.setProperty("webdriver.gecko.driver", "/Users/oksanaruban/Documents/Java/chromedriver"); //selenium 3.0

        driver = new FirefoxDriver();
        driver.get(baseUrl);
        driver.quit();
    }
}
