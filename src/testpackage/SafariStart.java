package testpackage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

public class SafariStart {
    public static void main(String[] args){
        WebDriver driver;

        System.setProperty("webdriver.gecko.driver", "/Users/oksanaruban/Documents/Java/geckodriver"); //selenium 3.0
        driver = new SafariDriver();
        String baseUrl = "https://nl.edreams.com";
        driver.get(baseUrl);
        //Thread.sleep(3000);
        //driver.quit();
    }
}
