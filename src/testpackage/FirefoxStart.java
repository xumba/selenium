package testpackage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxStart {
    public static void main(String[] args) {
        WebDriver driver;
        //driver = new FirefoxDriver();   Seleniun 2.0
        //String baseUrl = "https://nl.edreams.com";
        //driver.get(baseUrl);

        System.setProperty("webdriver.gecko.driver", "/Users/oksanaruban/Documents/Java/geckodriver"); //selenium 3.0
        driver = new FirefoxDriver();
        String baseUrl = "https://nl.edreams.com";
        driver.get(baseUrl);
        driver.quit();
    }
}
